// console.log("ohayou");

// What are conditional Statements
// Conditional statements allow us to control the flow of the program
    let numA= -1;
    if (numA<0){//true
        console.log("Hello")
    }
    console.log(numA<0)//True
/*
    Syntax:
    if(condition){
        Statement;
    }
*/
// The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

// lets update the variable and run if statement with the same condition.

    numA=0;
    if(numA<0){//false
        console.log("num a is 0")
    }
    let city = "New York";
    if(city === "New York"){//true
        console.log("Welcome to New York")
    }

//else if clause
/*
    -executes a statement if previous conditions are false and if the specified condition is true
    -The "else if" claus is optional and can be added to capture addition conditions
*/

    let numH = 1;
    if (numH < 0) {//false
    console.log("From numH");
    }
    else if(numH>0){//true
        console.log("Hi im numH");
    }

    numH = 1;
    if (numH > 0) {//false
    console.log("From numH");
    }
    else if(numH===1){//true
        console.log("Hi im the second condition met!");
    }
    else if(numH<0){//true
        console.log("Hi im numH");
    }

    city ="Tokyo";
    if (city==="New York"){//false
        console.log("Welcome to New York!");
    }
    else if(city==="Tokyo"){//true
        console.log("Welcome to Tokyo, Japan!");
    }

// else Statement
/*
    -Executes a statement if all other conditions are false / not met
    -else statement is optional and can be added to capture any other  result
    to change the flow of a program.
*/
    numH = 2;
    if (numH<0){//false
        console.log("Hello I'm numH");
    }
    else if (numH>2){//false
        console.log("numH is greater than 2")
    }else if (numH>3){//false
        console.log("numH is Greater than 3")
    }else{
        console.log("numH from else");
    };

//"else" and "else if" will not run w/o "I
/* 
{
    else if (condition){
        // statement;
    }
    else{
        console.log("Will not run w/o IF")
    }
}*/

// if, else if and else statement with function
/* 
    -Most of the times we would like to use if, else if, and else statements with function to control the flow  of our application;
*/

    let message;
    function determineTyphoonIntensity(windSpeed){
        if (isNaN(windSpeed))
        {
            console.warn("windSpeed isNaN");
            return "is Not a valid wind speed.";
        }
        if(windSpeed<30){
            return "Not a typhoon yet.";
        }
        else if(windSpeed<=60){
            return "Tropical depression detected.";
        }
        else if (windSpeed <=61 && windSpeed<=88){
            return "Tropical Storm detected.";
        }
        else if(windSpeed>=89 && windSpeed<=111){
            return "Severe Tropical storm detected.";
        }
        else{
            return "Typhoon detected.";
        }
    }
    message = determineTyphoonIntensity("999");
        console.log(message);
    if (message==="Typhoon detected."){
        console.error(message);
    }
    if ("name"){
        console.warn("string 'name' is true?");
    }



    // truthy examples
    if(true){
        console.log("Truthy");
    }
    if(1){
        console.log("Truthy");
    }
    if([]){
        console.log("Truthy");
    }
    // falsy
    if(false){
        console.log("Falsy");
    }
    if(0){
        console.log("Falsy");
    }
    if(undefined){
        console.log("Falsy");
    }
    // [Section] Conditional (ternary) Operator
    /*
        (expression)?ifTrue:ifFalse;
        1. Condition
        2. Expression to execute if the condition is truthy
        3. Expression if the condition is falsy
    */
    let ternaryResult = (1<18)?true:false;
    console.log("Result of ternaryResult: "+ternaryResult);

    // Multiple statement execution
    let name;

    function isOfLegalAge(){
        name= "john";
        return "You are Legal Age";
    }
    function isUnderAge(){
        name='Jane';
        return "You are under age limit";
    }
    // parseInt Convert obj to Integer
    let age = parseInt(prompt("What is your age?"));
    console.log(age)
    let legalAge = (age>=18)?isOfLegalAge(): isUnderAge();
    console.log("Result of Ternary Operator in Function: "+legalAge +", " + name);

// [Section] Switch Statement
    /*
        
    */
   let day;
   switch(day){
    case "sunday":
        console.log("Sun day");
    break;
    case "monday":
        console.log("Moon Day");
        break;
    case"tuesday":
        console.log("Mars Day");
        break;
    case"wednesday":
        console.log("Mercury Day");
        break;
    case"thursday":
        console.log("Jupiter Day");
        break;
    case"friday":
        console.log("Venus Day");
        break;
    case"saturday":
        console.log("Saturn Day");
        break;
    default:
        console.log("Please input a valid day");
        break;
   } 
day = prompt("What day of the week is it today?").toLowerCase();


// [Section] try-catch-finally statement
   function showIntesityAlert(windSpeed){
    try {
        alerat(determineTyphoonIntensity(windSpeed))
    }
    catch(err){
        console.error(err.message);
    }
    finally{
        alert("Intenstity update will show alert");
    }
   }
   showIntesityAlert(110);